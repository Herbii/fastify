const fastify = require('fastify')({ logger: true });
const { createClient } = require('redis');

const client = createClient({ url: 'redis://redis:6379' });

// Функция для генерации и кеширования массива случайных чисел
async function generateAndCacheArray() {
  const arr = Array.from({ length: 100 }, () => Math.floor(Math.random() * 100) + 1).sort((a, b) => a - b);
  await client.del('array');
  for (let num of arr) {
    await client.rPush('array', num.toString());
  }
  return arr;
}

// Функция для получения кешированного массива из Redis
async function getCachedArray() {
  const arr = await client.lRange('array', 0, -1);
  return arr.map(Number);
}

// Функция для бинарного поиска значения в массиве
function binarySearch(arr, x) {
  let l = 0, r = arr.length - 1;
  while (l <= r) {
    const mid = Math.floor((l + r) / 2);
    if (arr[mid] === x) return mid;
    if (arr[mid] < x) l = mid + 1;
    else r = mid - 1;
  }
  return -1;
}

// Маршрут для поиска значения в массиве
fastify.get('/search/:value', async (request, reply) => {
  const value = parseInt(request.params.value, 10);
  const arr = await getCachedArray();
  const index = binarySearch(arr, value);
  reply.send({ index });
});

// Маршрут для регенерации и кеширования нового массива
fastify.get('/regenerate', async (request, reply) => {
  const arr = await generateAndCacheArray();
  reply.send({ array: arr });
});

// Функция для запуска сервера
const start = async () => {
  try {
    await client.connect();
    const arrayExists = await client.exists('array');
    if (!arrayExists) {
      await generateAndCacheArray();
    }
    await fastify.listen({ port: 8080, host: '0.0.0.0' });
    fastify.log.info(`Server listening on ${fastify.server.address().port}`);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
